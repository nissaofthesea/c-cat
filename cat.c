// PUBLIC DOMAIN

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 64KiB buffer
#define BUFFER_LEN (1024 * 64)
uint8_t buffer[BUFFER_LEN] = {0};

#define STDOUT_NAME "standard output"
#define STDIN_NAME "standard input"

const char *const USAGE =
	"usage: %s [option]... [file]...\n"
	"concatenates input files to stdout.\n"
	"input defaults to stdin.\n"
	"\n"
	"options:\n"
	"  -h, --help  print help message\n";

struct Cat {
	uint8_t *const buffer;
	size_t const buffer_len;
	size_t len; // must not exceed capacity
	FILE *const out_file;
};

enum Error {
	E_OK,
	E_READ,
	E_WRITE,
	E_OPEN,
	E_CLOSE,
};

struct Cat cat_init(uint8_t *const buffer, const size_t buffer_len, FILE *const out_file) {
	assert(buffer_len != 0 && "buffer length must be nonzero");

	struct Cat cat = {
		.buffer = buffer,
		.buffer_len = buffer_len,
		.len = 0,
		.out_file = out_file,
	};

	// disable auto-buffering for output, we buffer it manually
	setbuf(cat.out_file, NULL);

	return cat;
}

enum Error cat_flush(struct Cat *const cat) {
	cat->len -= fwrite(cat->buffer, sizeof(uint8_t), cat->len, cat->out_file);
	if (cat->len || fflush(cat->out_file)) {
		return E_WRITE;
	}
	return E_OK;
}

enum Error cat_append(struct Cat *const cat, FILE *const file) {
	bool keep_reading = true;
	while (keep_reading) {
		assert(cat->buffer_len != 0);
		assert(cat->len <= cat->buffer_len);

		uint8_t *const remain_ptr = cat->buffer + cat->len;
		const size_t remain_len = cat->buffer_len - cat->len;
		cat->len += fread(remain_ptr, sizeof(uint8_t), remain_len, file);

		if (ferror(file)) {
			return E_READ;
		} else if (feof(file)) {
			keep_reading = false;
		}

		// buffer hit cap, flush
		assert(cat->len <= cat->buffer_len);
		if (cat->len == cat->buffer_len) {
			enum Error err = cat_flush(cat);
			if (err != E_OK) {
				return err;
			}
		}
	}

	return E_OK;
}

bool error_fmt(enum Error err, struct Cat *const cat, const char *const in_name) {
	(void)cat_flush(cat);

	switch (err) {
	case E_OK:
		break;
	case E_READ:
		fprintf(stderr, "error: failed to read file '%s' (%s)\n", in_name, strerror(errno));
		assert(in_name != NULL);
		break;
	case E_WRITE:
		fprintf(stderr, "error: failed to write to %s (%s)\n", STDOUT_NAME, strerror(errno));
		break;
	case E_OPEN:
		fprintf(stderr, "error: failed to open file '%s' (%s)\n", in_name, strerror(errno));
		assert(in_name != NULL);
		break;
	case E_CLOSE:
		fprintf(stderr, "error: failed to close file '%s' (%s)\n", in_name, strerror(errno));
		assert(in_name != NULL);
		break;
	default:
		assert(false && "unreachable");
		break;
	}

	return err != E_OK;
}

bool cat_argv_files(const int argc, char **const argv, struct Cat *const cat) {
	if (argc == 1) {
		return error_fmt(cat_append(cat, stdin), cat, STDIN_NAME);
	}

	bool failure = false;
	for (int i = 1; i < argc; ++i) {
		const char *path = argv[i];

		// open input file
		FILE *file = fopen(path, "rb");
		if (file == NULL) {
			failure |= error_fmt(E_OPEN, cat, path);
			continue;
		}

		// disable auto-buffering for the file, we manage our own buffer
		setbuf(file, NULL);

		// read the file into the buffer, flushing if cap reached
		failure |= error_fmt(cat_append(cat, file), cat, path);

		// close file regardless of whether appending to buffer succeeded
		if (fclose(file)) {
			failure |= error_fmt(E_CLOSE, cat, path);
			continue;
		}
	}

	return failure;
}

int main(int argc, char **argv) {
	if (2 <= argc) {
		if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
			printf(USAGE, argv[0]);
			return 0;
		}
	}

	struct Cat cat = cat_init(buffer, BUFFER_LEN, stdout);

	bool failure = false;
	failure |= cat_argv_files(argc, argv, &cat);
	failure |= error_fmt(cat_flush(&cat), &cat, NULL);
	if (failure) {
		return EXIT_FAILURE;
	}

	return 0;
}
