BIN_RELEASE := cat
BIN_DBG := cat_dbg
SRC := cat.c

CC := cc
CFLAGS := -std=c17 -Wall -Wextra -Wpedantic

.PHONY: clean debug release all

debug: CFLAGS += -DDEBUG -g -fsanitize=address,undefined,leak
debug: $(BIN_DBG)
$(BIN_DBG): $(SRC)
	$(CC) -o $(BIN_DBG) $(CFLAGS) $(SRC)

release: CFLAGS += -Werror -O3
release: $(BIN_RELEASE)
$(BIN_RELEASE): $(SRC)
	$(CC) -o $(BIN_RELEASE) $(CFLAGS) $(SRC)

all: debug release

clean:
	rm -fv $(BIN_RELEASE)
	rm -fv $(BIN_DBG)
